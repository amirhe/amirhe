<img src="https://gitlab.com/amirhe/amirhe/-/raw/main/HEADER.svg"></img>

<br>

⚡ If **premature optimization** is the root of all evil, Then I have an **evil factory**.  
💬 If you are searching for **new idea**, there you are.  
⚡ I explain things in a **complex** and **precise** manner **from the beginning**.  
🚒 **Kryptonite** and **misconceptions** about other's profession are two of my **main weaknesses**.  
🎨 I occasionally **draw** somethings.    

<p align="center">
 <img src="https://github.com/amirHossein-Ebrahimi/amirHossein-Ebrahimi/raw/main/commits.png" width="550" >  | 
<a href="https://t.me/AhIMi_channel" title="AhIMi channel" target="_blank"><img width="100" src="https://github.com/amirHossein-Ebrahimi/amirHossein-Ebrahimi/raw/main/AhIMi.png" alt="AhIMi"></a>  |  
<a href="https://twitter.com/realamirhe" title="twitter" target="_blank"><img width="100" src="https://brandeps.com/icon-download/T/Twitter-icon-vector-04.svg" alt="twitter"></a>
</p>


❤️ `react` `neo4j` `PyTorch` `spiking neural network`


<br><br>


<p align="center">
<a href="https://gitlab.com/amirhe"><img width="30px" src="https://simpleicons.org/icons/gitlab.svg" alt="GitLab"><a><span>&nbsp;</span><span>&nbsp;</span>
<a href="https://github.com/amirHossein-Ebrahimi"><img width="30px" src="https://simpleicons.org/icons/github.svg" alt="GitHub"><a><span>&nbsp;</span><span>&nbsp;</span>
<a href="https://keybase.io/amirhe"><img width="30px" src="https://simpleicons.org/icons/keybase.svg" alt="Keybase"><a><span>&nbsp;</span><span>&nbsp;</span>
</p>

